# Color Codes

Check [Colorize outputs](https://rubyfu.net/module-0x1-or-basic-ruby-kung-fu/string#colorize-your-outputs)

```
\033  [0;  31m
 ^     ^    ^    
 |     |    |
 |     |    |--------------------------------------- [The color number]
 |     |-------------------- [The modifier]            (ends with "m")
 |-- [Escaped character]           | 0 - normal                     
     (you can use "\e")            | 1 - bold
                                   | 2 - normal again
                                   | 3 - background color
                                   | 4 - underline
                                   | 5 - blinking
```

# Date

Check [DateTime](https://ruby-doc.org/stdlib-2.6.1/libdoc/date/rdoc/DateTime.html#method-c-now) (subclass of Date class)

Format date with [strftime](https://ruby-doc.org/stdlib-2.6.1/libdoc/date/rdoc/DateTime.html#method-i-strftime)
