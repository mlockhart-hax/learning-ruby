 ~/bin$ cat 2fa_disable_admin_note
#!/bin/bash
# CREATES ADMIN NOTE FOR 2FA DISABLE REQUESTS

YELLOW='\033[1;33m'
RED='\033[0;31m'
NC='\033[0m' # No Color

echo "Ticket link: "
read TICKET

echo "Target username for chatops verification: "
read USER

echo -e "${RED}`date '+%Y-%m-%d'` | 2FA removed | user requested | $TICKET${NC}"

echo "Verification"
echo -e "${YELLOW}/chatops run user find $USER${NC}"