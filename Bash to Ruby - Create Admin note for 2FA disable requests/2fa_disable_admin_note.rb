# CREATES ADMIN NOTE FOR 2FA DISABLE REQUESTS

class String
    def red; colorize(self, "\e[0m\e[31m"); end
    def yellow; colorize(self, "\e[0m\e[33m"); end
    def colorize(text, color_code) "#{color_code}#{text}\e[0m" end
end

require 'date'
date = DateTime.now

puts "Ticket link: "
TICKET = gets

puts "Target username for chatops verification: "
USER = gets

puts "#{date.strftime("%Y-%m-%d")} | 2FA removed | user requested | #{TICKET}".red

puts "Verification: "
puts "/chatops run user find #{USER}".yellow