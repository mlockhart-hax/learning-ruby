#Given a sorted array of numbers, remove any numbers that are divisible by 13. Return the amended array.
#
#Examples
#unlucky_13([53, 182, 435, 591, 637]) ➞ [53, 435, 591]
# 182 and 637 are divisible by 13.


ayseguls_numbers = [53, 182, 435, 591, 637, 13, 14]

def unlucky_13  (numbers)
    if numbers.kind_of? Array
        numbers.delete_if { |a| a % 13 == 0 }	
        return numbers
    else
        puts 'wat?'
    end
end

unlucky_13 ayseguls_numbers
