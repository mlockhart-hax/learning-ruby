# learning-ruby



- Some notes in [the project wiki](https://gitlab.com/mlockhart-hax/learning-ruby/-/wikis/home)
- investigations/questions in [the issues](https://gitlab.com/mlockhart-hax/learning-ruby/-/issues/)
- examples in [repository files](https://gitlab.com/mlockhart-hax/learning-ruby/-/tree/main)
- source code to [jit](https://github.com/jcoglan/jit)[GitHub] from the book [Building Git](https://shop.jcoglan.com/building-git/) by John Coglan