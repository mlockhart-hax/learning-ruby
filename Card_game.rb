# Card game: https://www.codewars.com/kata/633874ed198a4c00286aa39d
# Twelve cards with grades from 0 to 11 randomly divided among 3 players: Frank, Sam, and Tom, 4 cards each. 
#The game consists of 4 rounds. The goal of the round is to move by the card with the most points.
# In round 1, the first player who has a card with 0 points, takes the first turn, and he starts with 
#that card. Then the second player (queue - Frank -> Sam -> Tom -> Frank, etc.) can move with any of 
#his cards (each card is used only once per game, and there are no rules that require players to make 
#only the best moves). The third player makes his move after the second player, and he sees the previous moves.
# The winner of the previous round then makes the first move in the next round with any remaining card.
# The player who wins 2 rounds first, wins the game.

# Task
# Return true if Frank has a chance of winning the game.
# Return false if Frank has no chance.

# Input
# 3 arrays of 4 unique numbers in each (numbers in array are sorted in ascending order). 
#Input is always valid, no need to check.


Cards = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
Cards = Cards.shuffle

def compare(a, b, c)
    if a > b and a > c
        a
    elsif b > a and b > c
        b
    else c > a and c > b
        c   
    end 
end


Frank = Cards[0..3].sort
Sam = Cards[4..7].sort 
Tom = Cards[8..11].sort

p Cards
p Frank
p Sam
p Tom
f_win = 0
s_win = 0
t_win = 0
winner_round = 0

def get_cards(a, b, c)
    puts "#{a} starts. Throw a card"
    card1 = gets.chomp
    
    puts "#{b} throw a card"
    card2 = gets.chomp 
    
    puts "#{c} throw a card"
    card3 = gets.chomp
    
    winner_r = compare(card1, card2, card3)
    puts "Biggest number is #{winner_r}"

    if a.include? winner_r 
        a
        a - [card1]
        p a
        b.delete(card2)
        p b
        c.delete(card3)
        p c
    elsif b.include? winner_r
        return b
        a - [card1]
        p a
        b.delete(card2)
        p b
        c.delete(card3)
        p c
    else
        return c
        a - [card1]
        p a
        b.delete(card2)
        p b
        c.delete(card3)
        p c
    end
end

for i in 0..3 do 
    p "round #{i} starts"
    if i < 1 
        if Frank.include? 0
            winner_round = get_cards(Frank, Sam, Tom)
        elsif Sam.include? 0
            winner_round = get_cards(Sam, Tom, Frank)
        else
            winner_round = get_cards(Tom, Frank, Sam)
        end
    else
        if Frank.include? winner_round
            f_win = f_win + 1
            p "Frank won #{f_win} times"
            winner_round = get_cards(Frank, Sam, Tom)
        elsif Sam.include? winner_round
            s_win = s_win + 1
            p "Sam  won #{s_win} times"
            winner_round = get_cards(Sam, Tom, Frank)
        else 
            t_win = t_win + 1
            p "Tom  won #{t_win} times" 
            winner_round = get_cards(Tom, Frank, Sam)
        end
    end

end

